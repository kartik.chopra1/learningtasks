As per the tasks , I have used two services : Apache2 and nginx. 
Apache2 is running on all the three nodes, Nginx is to be running on one of the nodes. 

both the server are show a simple webpage( code below )

```
<!DOCTYPE HTML>
<html>

<body>


  <script>
    alert( 'the webserver is working just fine! ' );
  </script>

 

</body>

</html>
```

Apache2 is running on port 80 whereas nginx is running on port 8080

Dockerfile to build the image of apache2 service is as follows : 

```
FROM ubuntu
RUN apt-get update && apt-get install -y --no-install-recommends apt-utils 
RUN apt-get install -y apache2 
RUN apt-get update
COPY sh1.sh /home/sh1.sh
RUN chmod 700 /home/sh1.sh
EXPOSE 80
RUN mv /var/www/html/index.html /var/www/html/indexcopy.html
COPY index.html /var/www/html/index.html
ENTRYPOINT /home/sh1.sh ; /bin/bash 
```

Where sh1.sh is a script to start apache2 server as soon as the container of this image is launched. 
sh1.sh
```
#/bin/bash!
service apache2 start
```

Similarly for nginx the dockerfile is given below 

```
FROM ubuntu
RUN apt-get update && apt-get install -y --no-install-recommends apt-utils 
RUN apt-get install -y nginx
RUN apt-get update
RUN mv /etc/nginx/sites-enabled/default /etc/nginx/sites-enabled/default-copy
COPY default /etc/nginx/sites-enabled/default
EXPOSE 8080
COPY index.html /var/www/html/index.html
COPY sh2.sh /home/sh2.sh
RUN chmod 700 /home/sh2.sh
ENTRYPOINT /home/sh2.sh ; /bin/bash 

#CMD ["nginx", "-g", "daemon off;"]
```
the script sh2.sh required to start nginx server at the launching of the container is 

```
#/bin/bash
service nginx start
```


Build Commands : 

```
sudo docker build -t apachedemo -f apachedf .
```

```
sudo docker build -t nginxdemo -f nginxdf .
```

Three Vagrant machines have been setup , with the following IP's : 
```
Manager node : 192.168.33.10
Worker1 : 192.168.33.11
Worker2 : 192.168.33.12
```


Command to initialize swarm at manager node is : 
```
sudo docker swarm init --advertise-addr 192.168.33.10
```
the token genereated from this command was furthur used to add the nodes into the cluster is worker1 and worker2

Once the setup is done, the services were created on the manager node as per requirement 

Command to host the apache2 service on the three nodes : 
```
sudo docker service create --name “apache2demo” -p 90:80 --mode global apache2demo
```
Note : global is chosen here over the replicas flag as there are three nodes and we wanted the service on the three nodes. 

Command to host the nginx service on a single node at an instance : 
```
sudo docker service create --name “nginx demo” -p 91:8080 nginxdemo
```
